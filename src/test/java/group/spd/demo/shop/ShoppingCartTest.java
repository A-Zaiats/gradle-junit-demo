package group.spd.demo.shop;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Shopping Cart")
class ShoppingCartTest {

	private ShoppingCart shoppingCart;

	@BeforeEach
	void createCart() {
		shoppingCart = new ShoppingCart();
	}

	@Test
	@DisplayName("The count for an empty cart should be 0")
	void zeroCountForEmpty() {
		assertEquals(0, shoppingCart.getItemCount());
	}

	@Test
	@DisplayName("Total price for empty cart should be 0")
	void zeroPriceForEmpty() {
		assertEquals(0, shoppingCart.getTotalPrice());
	}

	@Nested
	@DisplayName("If I add some items to cart")
	class NotEmptyCart {

		@BeforeEach
		void addItemsToCart() {
			shoppingCart.addItem(new Item("Cola", 10));
			shoppingCart.addItem(new Item("Chips", 15));
		}

		@Test
		@DisplayName("total items should be equal the count of added items")
		void totalItems() {
			assertEquals(2, shoppingCart.getItemCount());
		}

		@Test
		@DisplayName("Total price should be equal sum of all item prices")
		void totalPrice() {
			assertAll(
					() -> assertEquals(25, shoppingCart.getTotalPrice()),
					() -> assertEquals(4, 2 * 2, "Дважды два четыре, это всем известно в целом мире")
			);
		}

		@Nested
		@DisplayName("And then I try to remove some item")
		class DeleteItem {

			@Test
			@DisplayName("It should throw exception if I try to remove not existing item")
			void deleteItem() throws ItemNotFoundException {
				final Item itemForDeletion = new Item("Unknown item", 10);

				assertThrows(ItemNotFoundException.class, () -> shoppingCart.removeItem(itemForDeletion));
			}
		}
	}
}