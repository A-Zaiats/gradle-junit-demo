package group.spd.demo.shop;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

	private final List<Item> items = new ArrayList<>();

	public int getItemCount() {
		return items.size();
	}

	public int getTotalPrice() {
		int price = 0;

		for (Item item : items) {
			price += item.getPrice();
		}

		return price;
	}

	public void addItem(Item item) {
		items.add(item);
	}

	public void removeItem(Item item) throws ItemNotFoundException {
		if (!items.contains(item)) {
			throw new ItemNotFoundException();
		}

		items.remove(item);
	}
}
