package group.spd.demo.words;

public class PalindromeUtil {

	public static boolean isPalindrome(String word) {
		String reversed = "";

		for (int i = word.length() - 1; i >= 0; i--) {
			reversed += word.charAt(i);
		}

		return reversed.equals(word);
	}
}
